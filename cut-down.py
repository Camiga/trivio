# This file has been cut-down to support Python IDLE on Windows 10 1709.

# Python trivia.

# re module used to remove characters that match a punctuation from answers.
from re import sub
# string module provides a shortcut to all available punctuation.
from string import punctuation as punct

# Each Q&A is stored in its own array, as part of the entire "storage" array.
storage = [
    ['Which version of Windows ended support on January 14, 2020?', 'Windows 7'],
    ['Which version of Python was released on February 24, 2020?', '3.8.2'],
    ['The Firefox web browser is backed by which company?', 'Mozilla'],
    ['Which company introduced the Merlin telephone system?', 'AT&T'],
    ['What is Google\'s JavaScript engine (used in Chrome and Node.js) called?', 'V8'],
    ['The old Microsoft Edge had its own browser engine. What was it called?', 'EdgeHTML'],
    ['What does SSH stand for?', 'Secure Shell'],
    ['A penguin is the mascot of Linux. What is its name?', 'Tux'],
    ['What does the "TI" in "TI-84 Plus" stand for?', 'Texas Instruments'],
    ['Running "devmgmt.msc" in Windows opens what?', 'Device Manager']
]
# Also store state of questions, and which one we are up to.
right, wrong, question = 0, 0, 1

# Loop through each element in the "storage" array.
for item in storage:
    # The first element in the "item" array contains the question.
    trivia = item[0]
    # And the second element contains the answer.
    answer = item[1]
    # Format a string with the question number and a >> prompt.
    fstring = f'{question}. [{trivia}] >> '

    # Format answers to lowercase without punctuation, split between words.
    received = sub(f'[{punct}]', '', input(fstring).lower()).split(' ')
    expected = sub(f'[{punct}]', '', answer.lower()).split(' ')
    # Convert both answers to sets containing only words, and compare them.
    # The goal is to ensure that every word in the answer matches in any order.
    if len(set(received) & set(expected)) >= len(expected):
        # Increase score.
        right += 1
        print('+ 1\n')
    else:
        # Increase failures, and print answer.
        wrong += 1
        print(f'X ({answer})\n')

    # Increase the cosmetic question index on each run.
    question += 1

# Print endscreen to show the number of correct and incorrect answers.
print(f'\n--> Finished!\n => {right} correct.\n => {wrong} incorrect.')
# Prompt user to end the program.
input('Press ENTER to exit.')
