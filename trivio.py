# Python trivia.

# zlib-compressed game storage, no plaintext here.
from base64 import b64decode
from zlib import decompress
# os module used to recieve number of terminal columns and rows.
from os import get_terminal_size
# re module used to remove characters that match a punctuation RegEx string.
from re import sub
# string module provides a shortcut to all available punctuation.
from string import punctuation as punct

# The entire game is compressed using base64 layered ontop of zlib.
storage = b'eNpdkl9v0zAUxd/7Ka76wB8JolEmUYkHVAFjQyuaSLQ+O/ZtYrB9I1+7aZD47rvOOlT2FEf2+Z1zfL3rre7hgJEtBaA97GwwNDJgMGiA8zBQTCB731XIKk7w7vINrC5WF58+/t08Hf6w2D3n3E2pl9WoGCI6VCw0+b/CNs6Y1RnmfbWuVoumR7iyEfd0hBFbaKOgMYJlaJX+Lfp2gnH20eQHFaai3dIf65w6BThtgA0pkslaREmwW4zOBkgSZJBUCDxxQl/0m+ZFI2KVis83os7hS5ayB1XraIckF9FZUbzKpYFAPveRPIIKBn6QweoXvwatnENTcPfruQc5A1urIzHtE3w1HUKvRJ4YaAz/qj2yK5j9y1XZdMYqsutme/sYzxAy1PU1cCree4rlTI06R4S6R+cWGxiEmCWkdCm9vWJNqYzj1oZ8PBlZnoME5bEgmnw8cyiyZXOzLFXl+3Z9CXcu8/J/2waPkvYmcIrZY0i8+JlDsKGDpcGD73yqPOsZ8vRGSLKxzE+lAviCB6tlMCqoDuMDKe3fhQ=='
# Decode the base64 and decompress the remaining zlib.
binaryt = decompress(b64decode(storage))
# Finally, convert to UTF-8 and split each value into an array.
trivia_store = binaryt.decode('UTF-8').split('\n')

# Store state of questions, and which one we are up to.
right, wrong, question = 0, 0, 1

# Loop through each element in the "trivia_store" array.
for item in trivia_store:
    # Get the number of columns and rows in the current terminal.
    width, height = get_terminal_size()
    # Create a top border fitting the terminal size.
    top = '┌' + '─' * (width - 1) + '\n'
    # And a bottom one.
    bot = '└' + '─' * (width - 1)

    # Seperate the question and answer of each item.
    pair = item.split(';}A')
    # The first element in the new "pair" array contains the question.
    trivia = pair[0]
    # And the second element contains the answer.
    answer = pair[1]
    # Format a string with the question number and a >> prompt.
    fstring = f'│ {question}. [{trivia}] >> '

    # Format answers to lowercase without punctuation, split between words.
    received = sub(f'[{punct}]', '', input(top + fstring).lower()).split(' ')
    expected = sub(f'[{punct}]', '', answer.lower()).split(' ')
    # Convert both answers to sets containing only words, and compare them.
    # The goal is to ensure that every word in the answer matches in any order.
    if len(set(received) & set(expected)) >= len(expected):
        # Increase score.
        right += 1
        print(f'│ ✔\n{bot}')
    else:
        # Increase failures, and print answer.
        wrong += 1
        print(f'│ ✘ {answer}\n{bot}')

    # Increase the cosmetic question index on each run.
    question += 1

# Create endscreen to show the number of correct and incorrect answers.
end1 = f'\n┏━Finished!━┓\n┃ ✔ {right}'
end2 = f'\n┃ ✘ {wrong}\n┗{"━" * 11}┛'
# Glue both parts together in a print, which runs before game end.
print(end1 + end2)
